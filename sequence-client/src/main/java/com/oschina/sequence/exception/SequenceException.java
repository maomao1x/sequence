package com.oschina.sequence.exception;

/**
 * Created by limu on 15/10/12.
 */
public class SequenceException extends Exception {
    private static final long serialVersionUID = -1365496127666610867L;


    public SequenceException() {
        super();
    }


    public SequenceException(String msg) {
        super(msg);
    }

    public SequenceException(Throwable t) {
        super(t);
    }


    public SequenceException(String msg, Throwable t) {
        super(msg, t);
    }


}
